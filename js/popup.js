class App {
    constructor() {
        this.currentPage = document.querySelector('.page.rendered');

        this.addEventListeners();
    }

    switchPage(newPage) {
        this.currentPage.classList.remove('rendered');

        const header = document.querySelector('header');

        if (newPage == "home") header.classList.add('is-home');
        else header.classList.remove('is-home');

        this.currentPage = document.querySelector(`.page[data-page="${newPage}"]`);
        this.currentPage.classList.add('rendered');
    }

    addEventListeners() {
        document.querySelectorAll('.switch-page').forEach(button => {
            button.addEventListener('click', this.clickSwitchPageHandler.bind(this));
        });

        document.querySelector('.header__back').addEventListener('click', this.clickBackPageHandler.bind(this));
    
        document.querySelector('.passwords__sorting').addEventListener('click', evt => {
            evt.currentTarget.classList.toggle('reverse');
        });

        const inputLength = document.querySelector('.generate__slider-text');
        const sliderLength = document.querySelector('.generate__slider');

        sliderLength.addEventListener('input', evt => {
            inputLength.value = evt.currentTarget.value;
        });

        inputLength.addEventListener('change', evt => {
            let value = evt.currentTarget.value;

            if (value < 6) value = 6;
            if (value > 32) value = 32;

            sliderLength.value = value;
            evt.currentTarget.value = value;
        });
    }

    clickBackPageHandler() {
        this.switchPage(this.currentPage.dataset.parent);
    }

    clickSwitchPageHandler(evt) {
        const pageName = evt.currentTarget.dataset.page;
        this.switchPage(pageName);
    }
}

const app = new App();
